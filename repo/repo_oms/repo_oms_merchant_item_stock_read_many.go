package repo_oms

import (
	"database/sql"
)

func (r repoOMS) MerchantItemStockReadMany(merchantID uint64) ([]*MerchantItemStock, error) {
	var mw []*MerchantItemStock

	const query = `SELECT item_id, merchant_id, stock 
					FROM item_merchant_stock 
					WHERE merchant_id = $1`

	rows, err := r.db.Query(query, merchantID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(MerchantItemStock)
		err := rows.Scan(&m.ItemID, &m.MerchantID, &m.Stock)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
