package repo_oms

import (
	"database/sql"

	"gitlab.com/sea-eevee/backend/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_oms . Contract

type Contract interface {
	MerchantItemStockReadMany(merchantID uint64) ([]*MerchantItemStock, error)
	MerchantItemStockCreate(m MerchantItemStock) error
	MerchantItemStockUpdate(merchantID, itemID uint64, stockDiff int) error
	MerchantItemStockDelete(itemID uint64) error
	MerchantItemStockReadOne(itemID uint64) (*MerchantItemStock, error)
	GetMerchantItemRequest(merchantID uint64) ([]*MerchantItemRequest, error)
	GetMerchantWallet(merchantID uint64) (*MerchantWallet, error)
	GetMerchantTransaction(merchantID uint64) ([]*MerchantTransaction, error)
}

type repoOMS struct {
	db *sql.DB
}

func NewRepoOMS(param store.DBConn) (Contract, error) {
	db, err := store.NewPostgres(param)
	if err != nil {
		return nil, err
	}
	return &repoOMS{
		db: db,
	}, nil
}
