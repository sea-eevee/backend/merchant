package repo_oms

func (r repoOMS) MerchantItemStockUpdate(merchantID, itemID uint64, stockDiff int) error {

	const query = `UPDATE 
    				item_merchant_stock 
    				SET stock = stock + $3
    				WHERE item_id = $1 AND merchant_id = $2`

	_, err := r.db.Exec(query, itemID, merchantID, stockDiff)
	if err != nil {
		return err
	}
	return nil
}
