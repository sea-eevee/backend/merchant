package repo_oms

import (
	"fmt"
)

func (r repoOMS) MerchantItemStockCreate(m MerchantItemStock) error {
	fmt.Printf("%v\n", m)
	query := "INSERT INTO item_merchant_stock(merchant_id, item_id, stock) VALUES($1,$2, $3)"
	_, err := r.db.Exec(query, m.MerchantID, m.ItemID, m.Stock)
	return err
}
