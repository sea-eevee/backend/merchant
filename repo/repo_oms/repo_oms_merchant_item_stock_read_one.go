package repo_oms

import (
	"database/sql"
)

func (r repoOMS) MerchantItemStockReadOne(itemID uint64) (*MerchantItemStock, error) {
	var mw = new(MerchantItemStock)

	const query = `SELECT item_id, merchant_id, stock 
					FROM item_merchant_stock 
					WHERE item_id = $1`

	rows, err := r.db.Query(query, itemID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		err := rows.Scan(&mw.ItemID, &mw.MerchantID, &mw.Stock)
		if err != nil {
			return nil, err
		}
	}
	return mw, nil
}
