package repo_oms

func (r repoOMS) MerchantItemStockDelete(itemID uint64) error {
	const delStatement = `DELETE FROM item_merchant_stock WHERE item_id = $1;`
	_, err := r.db.Exec(delStatement, itemID)
	return err
}
