package repo_oms

import (
	"database/sql"
)

func (r repoOMS) GetMerchantTransaction(merchantID uint64) ([]*MerchantTransaction, error) {
	var mw []*MerchantTransaction

	const query = `SELECT o.id, o.customer_id, t.transaction_date 
					FROM orders o 
					    JOIN transaction t ON o.id = t.order_id 
					WHERE o.merchant_id = $1`

	rows, err := r.db.Query(query, merchantID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(MerchantTransaction)
		err := rows.Scan(&m.TransactionID, &m.CustomerID, &m.TransactionDate)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
