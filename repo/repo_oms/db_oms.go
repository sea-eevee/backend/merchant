package repo_oms

type MerchantItemStock struct {
	MerchantID uint64
	ItemID     uint64
	Stock      uint
}

type MerchantItemRequest struct {
	MerchantID uint64
	ItemID     uint64
	Quantity   uint
}

type MerchantWallet struct {
	MerchantID uint64
	Wallet     uint64
}

type MerchantTransaction struct {
	TransactionID   uint64 // == orderID, get detail from order_detail
	CustomerID      uint64 // get detail from customer_profile
	TransactionDate uint64
}
