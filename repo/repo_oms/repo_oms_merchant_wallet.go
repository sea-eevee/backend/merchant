package repo_oms

import (
	"database/sql"
)

func (r repoOMS) GetMerchantWallet(merchantID uint64) (*MerchantWallet, error) {
	var mw = new(MerchantWallet)

	const query = `SELECT merchant_id, real_wallet_balance 
					FROM merchant_wallet 
					WHERE merchant_id = $1`

	err := r.db.QueryRow(query, merchantID).Scan(&mw.MerchantID, &mw.Wallet)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	return mw, nil
}
