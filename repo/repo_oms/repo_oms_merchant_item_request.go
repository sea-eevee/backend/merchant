package repo_oms

import (
	"database/sql"
)

func (r repoOMS) GetMerchantItemRequest(merchantID uint64) ([]*MerchantItemRequest, error) {
	var mw []*MerchantItemRequest

	const query = `SELECT merchant_id, item_id, quantity 
					FROM merchant_request_item 
					WHERE merchant_id = $1`

	rows, err := r.db.Query(query, merchantID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(MerchantItemRequest)
		err := rows.Scan(&m.MerchantID, &m.ItemID, &m.Quantity)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
