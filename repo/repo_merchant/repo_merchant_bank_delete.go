package repo_merchant

func (r repoMerchant) BankDelete(merchantID, bankID uint64) error {
	const delStatement = `DELETE FROM merchant_bank_account WHERE id = $1 and merchant_id = $2;`
	_, err := r.db.Exec(delStatement, bankID, merchantID)
	return err
}
