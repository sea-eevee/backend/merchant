package repo_merchant

func (r repoMerchant) ItemUpdate(itemID uint64, item *ItemDetail) error {
	const sqlStatement = `
	UPDATE item_detail
	SET merchant_id = $1, display_name = $2, description = $3, price = $4, image = $5
	WHERE id = $6;`
	_, err := r.db.Exec(sqlStatement, item.MerchantID, item.DisplayName, item.Description, item.Price, item.Image, itemID)
	return err
}
