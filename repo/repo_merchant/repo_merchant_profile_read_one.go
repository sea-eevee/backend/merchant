package repo_merchant

import (
	"database/sql"
)

func (r repoMerchant) ProfileReadOne(merchantID uint64) (*MerchantProfile, *Location, error) {

	const query = `
	SELECT m.merchant_id, m.display_name, m.location_id, l.province, l.city, m.description, m.photo_url
	FROM merchant_profile AS m
	JOIN location AS l ON l.id = m.location_id
	WHERE m.merchant_id = $1`

	row := r.db.QueryRow(query, merchantID)

	m := new(MerchantProfile)
	l := new(Location)
	err := row.Scan(&m.MerchantID, &m.DisplayName, &l.LocationID, &l.Province, &l.City, &m.Description, &m.PhotoURL)
	if err == sql.ErrNoRows {
		return nil, nil, nil
	}
	if err != nil {
		return nil, nil, err
	}

	return m, l, nil
}
