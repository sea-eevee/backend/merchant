package repo_merchant

func (r repoMerchant) TransferInsert(transfer *MerchantTransfer) (insertedID uint64, err error) {
	query := "INSERT INTO merchant_transfer(merchant_id, bank_account_number, bank_name, amount) VALUES($1,$2, $3, $4) returning id;"
	err = r.db.QueryRow(query, transfer.MerchantID, transfer.BankAccountNumber, transfer.BankName, transfer.Amount).Scan(&insertedID)
	if err != nil {
		return 0, err
	}
	return insertedID, nil
}
