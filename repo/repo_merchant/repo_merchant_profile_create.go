package repo_merchant

func (r repoMerchant) ProfileCreate(profile *MerchantProfile) (err error) {
	query := "INSERT INTO merchant_profile(merchant_id, display_name, description, photo_url) VALUES($1,$2, $3, $4, $5);"
	_, err = r.db.Query(query, profile.MerchantID, profile.DisplayName, profile.Description, profile.PhotoURL)
	return err
}
