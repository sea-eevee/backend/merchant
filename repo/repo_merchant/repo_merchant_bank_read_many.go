package repo_merchant

import (
	"database/sql"
)

func (r repoMerchant) BankReadMany(merchantID uint64) ([]*MerchantBank, error) {
	var mw []*MerchantBank

	const query = `SELECT id, merchant_id, bank_name, bank_account_number 
					FROM merchant_bank_account 
					WHERE merchant_id = $1`

	rows, err := r.db.Query(query, merchantID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(MerchantBank)
		err := rows.Scan(&m.ID, &m.MerchantID, &m.BankName, &m.BankAccountNumber)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
