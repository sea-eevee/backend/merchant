package repo_merchant

func (r repoMerchant) ItemDelete(itemID uint64) error {
	const delStatement = `DELETE FROM item_detail WHERE id = $1;`
	_, err := r.db.Exec(delStatement, itemID)
	return err
}
