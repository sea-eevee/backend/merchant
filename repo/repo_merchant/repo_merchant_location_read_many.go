package repo_merchant

import "database/sql"

func (r repoMerchant) LocationReadMany() ([]*Location, error) {
	var locs []*Location

	const query = `SELECT id, province, city FROM location`

	rows, err := r.db.Query(query)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		l := new(Location)
		err := rows.Scan(&l.LocationID, &l.Province, &l.City)
		if err != nil {
			return nil, err
		}
		locs = append(locs, l)
	}

	return locs, nil
}
