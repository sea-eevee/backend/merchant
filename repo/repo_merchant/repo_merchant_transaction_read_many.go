package repo_merchant

import "database/sql"

func (r repoMerchant) TransactionReadMany(merchantID uint64) ([]*MerchantTransactionHistory, error) {
	var mw []*MerchantTransactionHistory

	const query = `SELECT id, merchant_id, transaction_id, total_price
					FROM merchant_transaction_history
					WHERE merchant_id = $1`

	rows, err := r.db.Query(query, merchantID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(MerchantTransactionHistory)
		err := rows.Scan(&m.ID, &m.MerchantID, &m.TransactionID, &m.TotalPrice)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
