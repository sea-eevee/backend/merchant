package repo_merchant

func (r repoMerchant) ItemReadOne(itemID uint64) (*ItemDetail, error) {
	m := new(ItemDetail)

	const query = `SELECT id, merchant_id, display_name, price, description, image
					FROM item_detail 
					WHERE id = $1`

	err := r.db.QueryRow(query, itemID).Scan(&m.ItemID, &m.MerchantID, &m.DisplayName, &m.Price, &m.Description, &m.Image)
	if err != nil {
		return nil, err
	}
	return m, nil
}
