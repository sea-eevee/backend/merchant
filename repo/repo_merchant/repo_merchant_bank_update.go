package repo_merchant

func (r repoMerchant) BankUpdate(bank *MerchantBank) error {
	const sqlStatement = `
	UPDATE merchant_bank_account
	SET bank_name = $1, bank_account_number = $2
	WHERE id = $3 AND merchant_id = $4;`
	_, err := r.db.Exec(sqlStatement, bank.BankName, bank.BankAccountNumber, bank.ID, bank.MerchantID)
	return err
}
