package repo_merchant

func (r repoMerchant) BankCreate(bank *MerchantBank) (insertedID uint64, err error) {
	query := "INSERT INTO merchant_bank_account(merchant_id, bank_name, bank_account_number) VALUES($1,$2, $3) returning id;"
	err = r.db.QueryRow(query, bank.MerchantID, bank.BankName, bank.BankAccountNumber).Scan(&insertedID)
	if err != nil {
		return 0, err
	}
	return insertedID, nil
}
