package repo_merchant

import "database/sql"

func (r repoMerchant) TransferReadMany(merchantID uint64) ([]*MerchantTransfer, error) {
	var mw []*MerchantTransfer

	const query = `SELECT id, merchant_id, bank_account_number, bank_name, amount
					FROM merchant_transfer 
					WHERE merchant_id = $1`

	rows, err := r.db.Query(query, merchantID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(MerchantTransfer)
		err := rows.Scan(&m.TransferID, &m.MerchantID, &m.BankAccountNumber, &m.BankName, &m.Amount)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
