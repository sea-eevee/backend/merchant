package repo_merchant

func (r repoMerchant) ProfileUpdate(profile *MerchantProfile) error {
	const sqlStatement = `
	UPDATE merchant_profile
	SET display_name = $1, description = $2, photo_url = $3, location_id = $4
	WHERE merchant_id = $5;`
	_, err := r.db.Exec(sqlStatement, profile.DisplayName, profile.Description, profile.PhotoURL, profile.LocationID, profile.MerchantID)
	return err
}
