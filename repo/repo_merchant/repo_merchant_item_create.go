package repo_merchant

func (r repoMerchant) ItemCreate(item *ItemDetail) (insertedID uint64, err error) {
	query := "INSERT INTO item_detail(merchant_id, display_name, price, description, image) VALUES($1,$2, $3, $4, $5) returning id;"
	err = r.db.QueryRow(query, item.MerchantID, item.DisplayName, item.Price, item.Description, item.Image).Scan(&insertedID)
	if err != nil {
		return 0, err
	}
	return insertedID, nil
}
