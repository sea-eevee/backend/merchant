package repo_merchant

import (
	"database/sql"

	"gitlab.com/sea-eevee/backend/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_merchant . Contract

type Contract interface {
	ItemReadManyByMerchantID(merchantID uint64) ([]*ItemDetail, error)
	ItemReadOne(itemID uint64) (*ItemDetail, error)
	ItemCreate(item *ItemDetail) (insertedID uint64, err error)
	ItemUpdate(itemID uint64, item *ItemDetail) error
	ItemDelete(itemID uint64) error

	BankCreate(bank *MerchantBank) (insertedID uint64, err error)
	BankReadMany(merchantID uint64) ([]*MerchantBank, error)
	BankUpdate(bank *MerchantBank) error
	BankDelete(merchantID uint64, bankID uint64) error

	ProfileReadOne(merchantID uint64) (*MerchantProfile, *Location, error)
	ProfileUpdate(profile *MerchantProfile) error
	ProfileCreate(profile *MerchantProfile) error

	TransferInsert(transfer *MerchantTransfer) (uint64, error)
	TransferReadMany(merchantID uint64) ([]*MerchantTransfer, error)

	TransactionReadMany(merchantID uint64) ([]*MerchantTransactionHistory, error)

	LocationReadMany() ([]*Location, error)
}

type repoMerchant struct {
	db *sql.DB
}

func NewRepoMerchant(param store.DBConn) (Contract, error) {
	db, err := store.NewPostgres(param)
	if err != nil {
		return nil, err
	}
	return &repoMerchant{
		db: db,
	}, nil
}
