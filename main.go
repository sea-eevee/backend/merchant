package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/sea-eevee/backend/common/config"
	"gitlab.com/sea-eevee/backend/common/store"
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_oms"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
)

func main() {
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	cfg := config.InitAppConfig(path)

	dbConn := store.DBConn{
		//DBHost: "compfest-marketplace-service-merchant-db",
		DBHost: "localhost",
		//DBPort: cfg.DBPort,
		DBPort: 13200,
		DBUser: cfg.DBUser,
		DBPass: cfg.DBPass,
		DBName: cfg.DBName,
	}
	repoMerchant, err := repo_merchant.NewRepoMerchant(dbConn)
	if err != nil {
		log.Fatal(err)
	}
	repoOMS, err := repo_oms.NewRepoOMS(store.DBConn{
		//DBHost: "compfest-marketplace-service-oms-db",
		DBHost: "localhost",
		//DBPort: cfg.DBPort,
		DBPort: 13400,
		DBUser: cfg.DBUser,
		DBPass: cfg.DBPass,
		DBName: "oms",
	})
	ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
		RepoMerchant: repoMerchant,
		RepoOMS:      repoOMS,
	})
	r := NewRouterMerchant(ucMerchant)
	http.ListenAndServe(":8200", r)
}
