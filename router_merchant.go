package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/merchant/api_merchant"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
)

func NewRouterMerchant(ucMerchant uc_merchant.Contract) *mux.Router {
	r := mux.NewRouter()
	r.PathPrefix("/ping").HandlerFunc(ping)

	profile := r.PathPrefix("/profile").Subrouter()
	{
		profile.Methods(http.MethodGet).Handler(api_merchant.ProfileSee(ucMerchant.ProfileSee))
		profile.Methods(http.MethodPut).Handler(api_merchant.ProfileUpdate(ucMerchant.ProfileUpdate))
	}

	item := r.PathPrefix("/item").Subrouter()
	{
		item.Methods(http.MethodGet).Path("").Handler(api_merchant.MyItemReadMany(ucMerchant.MyItemReadMany))
		item.Methods(http.MethodPost).Path("").Handler(api_merchant.MyItemCreate(ucMerchant.MyItemCreate))

		itemSpecific := item.PathPrefix(fmt.Sprintf("/{%s:[0-9]+}", api_merchant.URLKeyItemID)).Subrouter()
		{
			itemSpecific.Methods(http.MethodGet).Path("").Handler(api_merchant.MyItemReadOne(ucMerchant.MyItemReadOne))
			itemSpecific.Methods(http.MethodDelete).Path("").Handler(api_merchant.MyItemDelete(ucMerchant.MyItemDelete))
			itemSpecific.Methods(http.MethodPut).Path("").Handler(api_merchant.MyItemUpdate(ucMerchant.MyItemUpdate))

			itemSpecific.Methods(http.MethodPut).Path(fmt.Sprintf("/stock/{%s:[0-9]+}", api_merchant.StockUpdate)).
				Handler(api_merchant.MyItemStockUpdate(ucMerchant.MyItemStockUpdate))
		}
	}

	transfer := r.PathPrefix("/transfer").Subrouter()
	{
		transfer.Methods(http.MethodGet).Handler(api_merchant.MyTransferReadMany(ucMerchant.MyTransferReadMany))
		transfer.Methods(http.MethodPost).Handler(api_merchant.MyTransferCreate(ucMerchant.MyTransferCreate))
	}

	r.Methods(http.MethodGet).Path("/transaction").
		Handler(api_merchant.MyTransactionReadMany(ucMerchant.MyTransactionReadMany))

	r.Methods(http.MethodGet).Path("/wallet").
		Handler(api_merchant.MyRealWalletBalanceRead(ucMerchant.MyRealWalletBalanceRead))

	r.Methods(http.MethodGet).Path("/requestitem").
		Handler(api_merchant.MyRequestItemReadMany(ucMerchant.MyRequestItemReadMany))

	r.Methods(http.MethodGet).Path("/location").
		Handler(api_merchant.MyLocationReadMany(ucMerchant.MyLocationReadMany))

	return r
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong from merchant service")
}
