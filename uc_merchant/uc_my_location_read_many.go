package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type MyLocationReadManyResponse struct {
	Locs []*repo_merchant.Location `json:"locations"`
}

type MyLocationReadManyUCFunc func() (*MyLocationReadManyResponse, error)

func (u ucMerchant) MyLocationReadMany() (*MyLocationReadManyResponse, error) {
	locs, err := u.repoMerchant.LocationReadMany()
	if err != nil {
		return nil, err
	}

	return &MyLocationReadManyResponse{Locs: locs}, nil
}
