package uc_merchant

type MyItemStockUpdateParam struct {
	MerchantID uint64
	ItemID     uint64
	Difference int // can be negative or positive
}

type MyItemStockUpdateResponse struct {
}

type MyItemStockUpdateUCFunc func(param *MyItemStockUpdateParam) (*MyItemStockUpdateResponse, error)

func (u ucMerchant) MyItemStockUpdate(param *MyItemStockUpdateParam) (*MyItemStockUpdateResponse, error) {
	err := u.repoOMS.MerchantItemStockUpdate(param.MerchantID, param.ItemID, param.Difference)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
