package uc_merchant

import "gitlab.com/sea-eevee/backend/merchant/pkg/dtos"

type MyRequestItemReadManyParam struct {
	MerchantID uint64
}

type MyRequestItemReadManyResponse struct {
	RequestItems []*dtos.RequestItem `json:"request_items"`
}

type MyRequestItemReadManyUCFunc func(param *MyRequestItemReadManyParam) (*MyRequestItemReadManyResponse, error)

func (u ucMerchant) MyRequestItemReadMany(param *MyRequestItemReadManyParam) (*MyRequestItemReadManyResponse, error) {
	items, err := u.repoOMS.GetMerchantItemRequest(param.MerchantID)
	if err != nil {
		return nil, err
	}
	var itemsResp []*dtos.RequestItem
	for _, item := range items {
		itemDetail, err := u.repoMerchant.ItemReadOne(item.ItemID)
		if err != nil {
			return nil, err
		}
		itemsResp = append(itemsResp, &dtos.RequestItem{
			Item: dtos.Item{
				ItemID:      itemDetail.ItemID,
				MerchantID:  itemDetail.MerchantID,
				Stock:       item.Quantity,
				DisplayName: itemDetail.DisplayName,
				Price:       itemDetail.Price,
				Description: itemDetail.Description,
				Image:       itemDetail.Image,
			},
			Quantity: item.Quantity,
		})
	}
	return &MyRequestItemReadManyResponse{RequestItems: itemsResp}, nil
}
