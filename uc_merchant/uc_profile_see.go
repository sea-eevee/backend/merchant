package uc_merchant

type ProfileSeeParam struct {
	MerchantID uint64 `json:"merchant_id"`
}

type LocationResp struct {
	LocationID uint64 `json:"location_id"`
	Province   string `json:"province"`
	City       string `json:"city"`
}

type ProfileSeeResponse struct {
	DisplayName string       `json:"display_name"`
	Location    LocationResp `json:"location"`
	Description string       `json:"description"`
	PhotoURL    string       `json:"photo_url"`
}

type ProfileSeeUCFunc func(param *ProfileSeeParam) (*ProfileSeeResponse, error)

func (u ucMerchant) ProfileSee(param *ProfileSeeParam) (*ProfileSeeResponse, error) {
	// TODO: implement me
	// SELECT * FROM merchant_profile m JOIN location l WHERE m.locationID = l.id
	mp, loc, err := u.repoMerchant.ProfileReadOne(param.MerchantID)
	if err != nil {
		return nil, err
	}
	return &ProfileSeeResponse{
		DisplayName: mp.DisplayName,
		Location: LocationResp{
			LocationID: loc.LocationID,
			Province:   loc.Province,
			City:       loc.City,
		},
		Description: mp.Description,
		PhotoURL:    mp.PhotoURL,
	}, nil
}
