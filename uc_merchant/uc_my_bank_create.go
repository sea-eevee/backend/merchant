package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type MyBankCreateParam struct {
	ID                uint64 `json:"id"`
	MerchantID        uint64 `json:"merchant_id"`
	BankName          string `json:"bank_name"`
	BankAccountNumber string `json:"bank_account_number"`
}

type MyBankCreateResponse struct {
	BankID uint64 `json:"bank_id"`
}

type MyBankCreateUCFunc func(param *MyBankCreateParam) (*MyBankCreateResponse, error)

func (u ucMerchant) MyBankCreate(param *MyBankCreateParam) (*MyBankCreateResponse, error) {
	insertedID, err := u.repoMerchant.BankCreate(&repo_merchant.MerchantBank{
		MerchantID:        param.MerchantID,
		BankAccountNumber: param.BankAccountNumber,
		BankName:          param.BankName,
	})
	if err != nil {
		return nil, err
	}
	return &MyBankCreateResponse{BankID: insertedID}, nil
}
