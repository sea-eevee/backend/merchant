package uc_merchant

type MyBankDeleteParam struct {
	ID         uint64
	MerchantID uint64
}

type MyBankDeleteResponse struct {
}

type MyBankDeleteUCFunc func(param *MyBankDeleteParam) (*MyBankDeleteResponse, error)

func (u ucMerchant) MyBankDelete(param *MyBankDeleteParam) (*MyBankDeleteResponse, error) {
	err := u.repoMerchant.BankDelete(param.MerchantID, param.ID)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
