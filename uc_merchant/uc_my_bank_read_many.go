package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type MyBankReadManyParam struct {
	MerchantID uint64
}

type MyBankReadManyResponse struct {
	Banks []*repo_merchant.MerchantBank `json:"banks"`
}

type MyBankReadManyUCFunc func(param *MyBankReadManyParam) (*MyBankReadManyResponse, error)

func (u ucMerchant) MyBankReadMany(param *MyBankReadManyParam) (*MyBankReadManyResponse, error) {
	banks, err := u.repoMerchant.BankReadMany(param.MerchantID)
	if err != nil {
		return nil, err
	}
	return &MyBankReadManyResponse{Banks: banks}, nil
}
