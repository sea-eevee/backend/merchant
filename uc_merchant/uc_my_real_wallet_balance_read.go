package uc_merchant

type MyRealWalletBalanceReadParam struct {
	MerchantID uint64
}

type MyRealWalletBalanceReadResponse struct {
	RealWalletBalance uint64 `json:"real_wallet_balance"`
}

type MyRealWalletBalanceReadUCFunc func(param *MyRealWalletBalanceReadParam) (*MyRealWalletBalanceReadResponse, error)

func (u ucMerchant) MyRealWalletBalanceRead(param *MyRealWalletBalanceReadParam) (*MyRealWalletBalanceReadResponse, error) {
	merchant_wallet, err := u.repoOMS.GetMerchantWallet(param.MerchantID)
	if err != nil {
		return nil, err
	}
	return &MyRealWalletBalanceReadResponse{RealWalletBalance: merchant_wallet.Wallet}, nil
}
