package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/pkg/dtos"
)

type MyItemReadOneParam struct {
	ItemID uint64
}

type MyItemReadOneResponse struct {
	Item *dtos.Item `json:"item"`
}

type MyItemReadOneUCFunc func(param *MyItemReadOneParam) (*MyItemReadOneResponse, error)

func (u ucMerchant) MyItemReadOne(param *MyItemReadOneParam) (*MyItemReadOneResponse, error) {
	item, err := u.repoMerchant.ItemReadOne(param.ItemID)
	if err != nil {
		return nil, err
	}
	itemStock, err := u.repoOMS.MerchantItemStockReadOne(item.ItemID)
	if err != nil {
		return nil, err
	}

	return &MyItemReadOneResponse{Item: &dtos.Item{
		Description: item.Description,
		DisplayName: item.DisplayName,
		Image:       item.Image,
		ItemID:      item.ItemID,
		MerchantID:  item.MerchantID,
		Price:       item.Price,
		Stock:       itemStock.Stock,
	}}, nil
}
