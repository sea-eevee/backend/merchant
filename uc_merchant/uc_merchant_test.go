package uc_merchant_test

import (
	"testing"

	"gitlab.com/sea-eevee/backend/merchant/repo/repo_oms"

	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"

	"gitlab.com/sea-eevee/backend/merchant/pkg/dtos"

	mock_repo_oms "gitlab.com/sea-eevee/backend/merchant/repo/repo_oms/mock"

	"github.com/golang/mock/gomock"
	mock_repo_merchant "gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant/mock"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
)

func TestCreateItem(t *testing.T) {
	tts := []struct {
		name             string
		expectedMockItem *uc_merchant.MyItemCreateResponse
		err              error
	}{
		{
			name:             "Success get categories",
			expectedMockItem: nil,
			err:              nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockParam := &uc_merchant.MyItemCreateParam{
				MerchantID: 1,
				ItemDetail: &dtos.Item{
					Description: "a",
					DisplayName: "b",
					Image:       "c",
					ItemID:      1,
					MerchantID:  1,
					Price:       1,
					Stock:       1,
				},
			}
			itemCreate := &repo_merchant.ItemDetail{
				MerchantID:  mockParam.MerchantID,
				Description: mockParam.ItemDetail.Description,
				DisplayName: mockParam.ItemDetail.DisplayName,
				Image:       mockParam.ItemDetail.Image,
				Price:       mockParam.ItemDetail.Price,
			}
			itemOMS := repo_oms.MerchantItemStock{
				ItemID:     1,
				MerchantID: 1,
				Stock:      1,
			}
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			mockOMS := mock_repo_oms.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
				RepoOMS:      mockOMS,
			})
			var id uint64 = 1
			mockRepo.EXPECT().ItemCreate(itemCreate).Return(id, nil)
			mockOMS.EXPECT().MerchantItemStockCreate(itemOMS).Return(nil)
			_, err := ucMerchant.MyItemCreate(mockParam)
			if err != nil {
				t.Fail()
			}
		})
	}
}

func TestDeleteItem(t *testing.T) {
	tts := []struct {
		name             string
		expectedMockItem *uc_merchant.MyItemDeleteResponse
		err              error
	}{
		{
			name:             "Success get categories",
			expectedMockItem: nil,
			err:              nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockParam := &uc_merchant.MyItemDeleteParam{
				ItemID:     1,
				MerchantID: 1,
			}
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			mockOMS := mock_repo_oms.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
				RepoOMS:      mockOMS,
			})
			mockRepo.EXPECT().ItemDelete(mockParam.ItemID).Return(nil)
			mockOMS.EXPECT().MerchantItemStockDelete(mockParam.ItemID).Return(nil)
			_, err := ucMerchant.MyItemDelete(mockParam)
			if err != nil {
				t.Fail()
			}
		})
	}
}

func TestReadManyItem(t *testing.T) {
	tts := []struct {
		name             string
		expectedMockItem *uc_merchant.MyItemReadManyResponse
		err              error
	}{
		{
			name:             "Success get categories",
			expectedMockItem: nil,
			err:              nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockParam := &uc_merchant.MyItemReadManyParam{
				MerchantID: 1,
			}
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
			})
			expectedManyItem := []*repo_merchant.ItemDetail{}
			mockRepo.EXPECT().ItemReadManyByMerchantID(mockParam.MerchantID).Return(expectedManyItem, nil)
			_, err := ucMerchant.MyItemReadMany(mockParam)
			if err != nil {
				t.Fail()
			}
		})
	}
}

func TestReadOneItem(t *testing.T) {
	tts := []struct {
		name             string
		expectedMockItem *uc_merchant.MyItemReadOneResponse
		err              error
	}{
		{
			name:             "Success get categories",
			expectedMockItem: nil,
			err:              nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockParam := &uc_merchant.MyItemReadOneParam{
				ItemID: 1,
			}
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			mockOms := mock_repo_oms.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
				RepoOMS:      mockOms,
			})
			expectedOneItem := &repo_merchant.ItemDetail{ItemID: 1}
			expectedStick := &repo_oms.MerchantItemStock{Stock: 2}
			mockRepo.EXPECT().ItemReadOne(mockParam.ItemID).Return(expectedOneItem, nil)
			mockOms.EXPECT().MerchantItemStockReadOne(expectedOneItem.ItemID).Return(expectedStick, nil)
			_, err := ucMerchant.MyItemReadOne(mockParam)
			if err != nil {
				t.Fail()
			}
		})
	}
}

func TestStockUpdateItem(t *testing.T) {
	stockUpdate := &uc_merchant.MyItemStockUpdateParam{
		ItemID:     1,
		MerchantID: 1,
		Difference: 5,
	}
	tts := []struct {
		name             string
		expectedMockItem *uc_merchant.MyItemStockUpdateResponse
		err              error
	}{
		{
			name:             "Success get categories",
			expectedMockItem: nil,
			err:              nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			mockOMS := mock_repo_oms.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
				RepoOMS:      mockOMS,
			})
			mockOMS.EXPECT().MerchantItemStockUpdate(stockUpdate.ItemID, stockUpdate.MerchantID, stockUpdate.Difference).Return(nil)
			_, err := ucMerchant.MyItemStockUpdate(stockUpdate)
			if err != nil {
				t.Fail()
			}
		})
	}
}

func TestUpdateItem(t *testing.T) {
	param := &uc_merchant.MyItemUpdateParam{
		MerchantID: 1,
		ItemID:     1,
		ItemDetail: &dtos.Item{
			Description: "a",
			DisplayName: "b",
			Image:       "c",
			Price:       1,
			Stock:       1,
		},
	}
	updateItem := &repo_merchant.ItemDetail{
		Description: "a",
		DisplayName: "b",
		Image:       "c",
		Price:       1,
		MerchantID:  1,
	}
	tts := []struct {
		name             string
		expectedMockItem *uc_merchant.MyItemUpdateResponse
		err              error
	}{
		{
			name:             "Success get categories",
			expectedMockItem: nil,
			err:              nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			mockOMS := mock_repo_oms.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
				RepoOMS:      mockOMS,
			})
			mockRepo.EXPECT().ItemUpdate(param.ItemID, updateItem).Return(nil)
			_, err := ucMerchant.MyItemUpdate(param)
			if err != nil {
				t.Fail()
			}
		})
	}
}

func TestRealWalletBalance(t *testing.T) {
	param := &uc_merchant.MyRealWalletBalanceReadParam{
		MerchantID: 1,
	}
	merchantWallet := &repo_oms.MerchantWallet{
		MerchantID: 1,
		Wallet:     10,
	}
	tts := []struct {
		name                string
		expectedBalanceResp *uc_merchant.MyRealWalletBalanceReadResponse
		err                 error
	}{
		{
			name: "Success get wallet balance",
			expectedBalanceResp: &uc_merchant.MyRealWalletBalanceReadResponse{
				RealWalletBalance: merchantWallet.Wallet,
			},
			err: nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			mockOMS := mock_repo_oms.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
				RepoOMS:      mockOMS,
			})
			mockOMS.EXPECT().GetMerchantWallet(param.MerchantID).Return(merchantWallet, nil)
			_, err := ucMerchant.MyRealWalletBalanceRead(param)
			if err != nil {
				t.Fail()
			}
		})
	}
}

func TestTranserCreate(t *testing.T) {
	param := &uc_merchant.MyTransferCreateParam{
		MerchantID:            1,
		MerchantBankAccountID: 1,
		Amount:                10,
	}
	merchantTransfer := &repo_merchant.MerchantTransfer{
		MerchantID:            1,
		MerchantBankAccountID: 1,
		Amount:                10,
	}
	tts := []struct {
		name                string
		expectedBalanceResp *uc_merchant.MyTransferCreateResponse
		err                 error
	}{
		{
			name: "Success get wallet balance",
			expectedBalanceResp: &uc_merchant.MyTransferCreateResponse{
				InsertedID: 1,
			},
			err: nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			mockOMS := mock_repo_oms.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
				RepoOMS:      mockOMS,
			})
			mockRepo.EXPECT().TransferInsert(merchantTransfer).Return(tt.expectedBalanceResp.InsertedID, nil)
			_, err := ucMerchant.MyTransferCreate(param)
			if err != nil {
				t.Fail()
			}
		})
	}
}

func TestProfileSee(t *testing.T) {
	param := &uc_merchant.ProfileSeeParam{
		MerchantID: 1,
	}
	merchantProfile := &repo_merchant.MerchantProfile{
		MerchantID:  1,
		Description: "a",
		DisplayName: "b",
		LocationID:  1,
		PhotoURL:    "x",
	}
	location := &repo_merchant.Location{
		City:       "y",
		LocationID: 1,
		Province:   "a",
	}
	tts := []struct {
		name        string
		profileResp *uc_merchant.ProfileSeeResponse
		err         error
	}{
		{
			name: "Success get wallet balance",
			profileResp: &uc_merchant.ProfileSeeResponse{
				Description: merchantProfile.Description,
				DisplayName: merchantProfile.DisplayName,
				PhotoURL:    merchantProfile.PhotoURL,
				Location: uc_merchant.LocationResp{
					City:       location.City,
					LocationID: location.LocationID,
					Province:   location.Province,
				},
			},
			err: nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			mockOMS := mock_repo_oms.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
				RepoOMS:      mockOMS,
			})
			mockRepo.EXPECT().ProfileReadOne(param.MerchantID).Return(merchantProfile, location, nil)
			_, err := ucMerchant.ProfileSee(param)
			if err != nil {
				t.Fail()
			}
		})
	}
}

func TestTransferReadMany(t *testing.T) {
	param := &uc_merchant.MyTransferReadManyParam{
		MerchantID: 1,
	}
	list_transfer := []*repo_merchant.MerchantTransfer{
		&repo_merchant.MerchantTransfer{
			Amount:                1,
			MerchantBankAccountID: 1,
			MerchantID:            1,
			TransferID:            1,
		},
	}
	tts := []struct {
		name         string
		transferResp *uc_merchant.MyTransferReadManyResponse
		err          error
	}{
		{
			name: "Success get wallet balance",
			transferResp: &uc_merchant.MyTransferReadManyResponse{
				Transfers: list_transfer,
			},
			err: nil,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockRepo := mock_repo_merchant.NewMockContract(mockCtrl)
			mockOMS := mock_repo_oms.NewMockContract(mockCtrl)
			ucMerchant := uc_merchant.NewUCMerchant(uc_merchant.UCMerchantParam{
				RepoMerchant: mockRepo,
				RepoOMS:      mockOMS,
			})
			mockRepo.EXPECT().TransferReadMany(param.MerchantID).Return(list_transfer, nil)
			_, err := ucMerchant.MyTransferReadMany(param)
			if err != nil {
				t.Fail()
			}
		})
	}
}
