package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type MyItemReadManyParam struct {
	MerchantID uint64
}

type MyItemReadManyResponse struct {
	Items []*repo_merchant.ItemDetail `json:"items"`
}

type MyItemReadManyUCFunc func(param *MyItemReadManyParam) (*MyItemReadManyResponse, error)

func (u ucMerchant) MyItemReadMany(param *MyItemReadManyParam) (*MyItemReadManyResponse, error) {
	items, err := u.repoMerchant.ItemReadManyByMerchantID(param.MerchantID)
	if err != nil {
		return nil, err
	}

	return &MyItemReadManyResponse{Items: items}, nil
}
