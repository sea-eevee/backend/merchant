package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/pkg/dtos"
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_oms"
)

type MyItemCreateParam struct {
	MerchantID uint64
	ItemDetail *dtos.Item
}

type MyItemCreateResponse struct {
	ItemID uint64
}

type MyItemCreateUCFunc func(param *MyItemCreateParam) (*MyItemCreateResponse, error)

func (u ucMerchant) MyItemCreate(param *MyItemCreateParam) (*MyItemCreateResponse, error) {
	// TODO: upload image to S3
	insertedID, err := u.repoMerchant.ItemCreate(&repo_merchant.ItemDetail{
		MerchantID:  param.MerchantID,
		DisplayName: param.ItemDetail.DisplayName,
		Price:       param.ItemDetail.Price,
		Description: param.ItemDetail.Description,
		Image:       param.ItemDetail.Image,
	})
	if err != nil {
		return nil, err
	}

	err = u.repoOMS.MerchantItemStockCreate(repo_oms.MerchantItemStock{
		MerchantID: param.MerchantID,
		ItemID:     insertedID,
		Stock:      param.ItemDetail.Stock,
	})
	if err != nil {
		return nil, err
	}
	return &MyItemCreateResponse{ItemID: insertedID}, nil
}
