package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type MyTransferCreateParam struct {
	MerchantID        uint64 `json:"merchant_id"`
	BankAccountNumber uint64 `json:"bank_account_number"`
	BankName          string `json:"bank_name"`
	Amount            uint   `json:"amount"`
}

type MyTransferCreateResponse struct {
	TransferID uint64 `json:"transfer_id"`
}

type MyTransferCreateUCFunc func(param *MyTransferCreateParam) (*MyTransferCreateResponse, error)

func (u ucMerchant) MyTransferCreate(param *MyTransferCreateParam) (*MyTransferCreateResponse, error) {
	transferID, err := u.repoMerchant.TransferInsert(&repo_merchant.MerchantTransfer{
		MerchantID:        param.MerchantID,
		BankAccountNumber: param.BankAccountNumber,
		BankName:          param.BankName,
		Amount:            param.Amount,
	})
	if err != nil {
		return nil, err
	}

	return &MyTransferCreateResponse{TransferID: transferID}, nil
}
