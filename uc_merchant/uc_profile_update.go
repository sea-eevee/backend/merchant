package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type ProfileUpdateParam struct {
	MerchantID  uint64 `json:"merchant_id" mapstructure:"merchant_id"`
	DisplayName string `json:"display_name" mapstructure:"display_name"`
	LocationID  uint64 `json:"location_id" mapstructure:"location_id"`
	Description string `json:"description" mapstructure:"description"`
	PhotoURL    string `json:"image" mapstructure:"image"`
}

type ProfileUpdateResponse struct {
	UpdatedDisplayName string `json:"updated_display_name"`
	UpdatedDescription string `json:"updated_description"`
	UpdatedPhotoURL    string `json:"updated_photo_url"`
	UpdatedLocationID  string `json:"updated_location_id"`
}

type ProfileUpdateUCFunc func(param *ProfileUpdateParam) (*ProfileUpdateResponse, error)

func (u ucMerchant) ProfileUpdate(param *ProfileUpdateParam) (*ProfileUpdateResponse, error) {
	profile, _, err := u.repoMerchant.ProfileReadOne(param.MerchantID)
	if err != nil {
		return nil, err
	}
	updatedProfile := &repo_merchant.MerchantProfile{
		MerchantID:  param.MerchantID,
		DisplayName: param.DisplayName,
		LocationID:  param.LocationID,
		Description: param.Description,
		PhotoURL:    param.PhotoURL,
	}
	if profile == nil {
		err = u.repoMerchant.ProfileCreate(updatedProfile)
	} else {
		err = u.repoMerchant.ProfileUpdate(updatedProfile)
	}
	if err != nil {
		return nil, err
	}
	return nil, nil
}
