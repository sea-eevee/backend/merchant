package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type MyBankUpdateParam struct {
	ID                uint64 `json:"id"`
	MerchantID        uint64 `json:"merchant_id"`
	BankName          string `json:"bank_name"`
	BankAccountNumber string `json:"bank_account_number"`
}

type MyBankUpdateResponse struct {
}

type MyBankUpdateUCFunc func(param *MyBankUpdateParam) (*MyBankUpdateResponse, error)

func (u ucMerchant) MyBankUpdate(param *MyBankUpdateParam) (*MyBankUpdateResponse, error) {
	err := u.repoMerchant.BankUpdate(&repo_merchant.MerchantBank{
		ID:                param.ID,
		MerchantID:        param.MerchantID,
		BankAccountNumber: param.BankAccountNumber,
		BankName:          param.BankName,
	})
	if err != nil {
		return nil, err
	}
	return nil, nil
}
