package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type MyTransactionReadManyParam struct {
	MerchantID uint64
}

type MyTransactionReadManyResponse struct {
	Transactions []*repo_merchant.MerchantTransactionHistory `json:"transactions"`
}

type MyTransactionReadManyUCFunc func(param *MyTransactionReadManyParam) (*MyTransactionReadManyResponse, error)

func (u ucMerchant) MyTransactionReadMany(param *MyTransactionReadManyParam) (*MyTransactionReadManyResponse, error) {
	ts, err := u.repoMerchant.TransactionReadMany(param.MerchantID)
	if err != nil {
		return nil, err
	}
	return &MyTransactionReadManyResponse{Transactions: ts}, nil
}
