package uc_merchant

type MyItemDeleteParam struct {
	MerchantID uint64
	ItemID     uint64
}

type MyItemDeleteResponse struct {
}

type MyItemDeleteUCFunc func(param *MyItemDeleteParam) (*MyItemDeleteResponse, error)

func (u ucMerchant) MyItemDelete(param *MyItemDeleteParam) (*MyItemDeleteResponse, error) {
	err := u.repoMerchant.ItemDelete(param.ItemID)
	if err != nil {
		return nil, err
	}
	err = u.repoOMS.MerchantItemStockDelete(param.ItemID)
	if err != nil {
		return nil, err
	}
	return nil, nil // success == nil
}
