package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/pkg/dtos"
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type MyItemUpdateParam struct {
	MerchantID uint64
	ItemID     uint64
	ItemDetail *dtos.Item
}

type MyItemUpdateResponse struct {
}

type MyItemUpdateUCFunc func(param *MyItemUpdateParam) (*MyItemUpdateResponse, error)

func (u ucMerchant) MyItemUpdate(param *MyItemUpdateParam) (*MyItemUpdateResponse, error) {
	err := u.repoMerchant.ItemUpdate(param.ItemID, &repo_merchant.ItemDetail{
		MerchantID:  param.MerchantID,
		DisplayName: param.ItemDetail.DisplayName,
		Price:       param.ItemDetail.Price,
		Description: param.ItemDetail.Description,
		Image:       param.ItemDetail.Image,
	})
	if err != nil {
		return nil, err
	}
	return nil, nil
}
