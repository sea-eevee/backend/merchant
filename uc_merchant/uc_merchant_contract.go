package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_oms"
)

type Contract interface {
	ProfileSee(param *ProfileSeeParam) (*ProfileSeeResponse, error)
	ProfileUpdate(param *ProfileUpdateParam) (*ProfileUpdateResponse, error)

	MyItemReadMany(param *MyItemReadManyParam) (*MyItemReadManyResponse, error)
	MyItemReadOne(param *MyItemReadOneParam) (*MyItemReadOneResponse, error)
	MyItemCreate(param *MyItemCreateParam) (*MyItemCreateResponse, error)
	MyItemUpdate(param *MyItemUpdateParam) (*MyItemUpdateResponse, error)
	MyItemDelete(param *MyItemDeleteParam) (*MyItemDeleteResponse, error)

	MyItemStockUpdate(param *MyItemStockUpdateParam) (*MyItemStockUpdateResponse, error)

	MyTransactionReadMany(param *MyTransactionReadManyParam) (*MyTransactionReadManyResponse, error)

	MyRealWalletBalanceRead(param *MyRealWalletBalanceReadParam) (*MyRealWalletBalanceReadResponse, error)

	MyRequestItemReadMany(param *MyRequestItemReadManyParam) (*MyRequestItemReadManyResponse, error)

	MyTransferReadMany(param *MyTransferReadManyParam) (*MyTransferReadManyResponse, error)
	MyTransferCreate(param *MyTransferCreateParam) (*MyTransferCreateResponse, error)

	MyBankCreate(*MyBankCreateParam) (*MyBankCreateResponse, error)
	MyBankReadMany(*MyBankReadManyParam) (*MyBankReadManyResponse, error)
	MyBankUpdate(*MyBankUpdateParam) (*MyBankUpdateResponse, error)
	MyBankDelete(*MyBankDeleteParam) (*MyBankDeleteResponse, error)

	MyLocationReadMany() (*MyLocationReadManyResponse, error)
}

type ucMerchant struct {
	repoMerchant repo_merchant.Contract
	repoOMS      repo_oms.Contract
}

type UCMerchantParam struct {
	RepoMerchant repo_merchant.Contract
	RepoOMS      repo_oms.Contract
}

func NewUCMerchant(param UCMerchantParam) Contract {
	return ucMerchant{
		repoMerchant: param.RepoMerchant,
		repoOMS:      param.RepoOMS,
	}
}
