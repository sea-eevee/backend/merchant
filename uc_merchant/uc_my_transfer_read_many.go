package uc_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
)

type MyTransferReadManyParam struct {
	MerchantID uint64
}

type MyTransferReadManyResponse struct {
	Transfers []*repo_merchant.MerchantTransfer `json:"transfers"`
}

type MyTransferReadManyUCFunc func(param *MyTransferReadManyParam) (*MyTransferReadManyResponse, error)

func (u ucMerchant) MyTransferReadMany(param *MyTransferReadManyParam) (*MyTransferReadManyResponse, error) {
	merchant_transfer, err := u.repoMerchant.TransferReadMany(param.MerchantID)
	if err != nil {
		return nil, err
	}
	return &MyTransferReadManyResponse{Transfers: merchant_transfer}, nil
}
