CREATE TABLE location (
  id BIGSERIAL PRIMARY KEY,
  province VARCHAR(255),
  city VARCHAR(255)
);
INSERT INTO location(province, city) VALUES ('DKI Jakarta', 'Jakarta Pusat');
INSERT INTO location(province, city) VALUES ('DKI Jakarta', 'Jakarta Selatan');
INSERT INTO location(province, city) VALUES ('Jawa Barat', 'Kota Depok');
INSERT INTO location(province, city) VALUES ('Jawa Barat', 'Kota Bogor');
INSERT INTO location(province, city) VALUES ('Banten', 'Kota Tangerang');
INSERT INTO location(province, city) VALUES ('Jawa Tengah', 'Kota Solo');
INSERT INTO location(province, city) VALUES ('Jawa Timur', 'Kota Surabaya');
select setval('location_id_seq'::regclass,8,false);


CREATE TABLE merchant_profile (
  merchant_id BIGINT NOT NULL, -- REFERENCES merchants (id),
  display_name VARCHAR(255) DEFAULT 'Lorem Ipsum',
  location_id BIGINT REFERENCES location (id),
  description varchar(255) DEFAULT 'Lorem Ipsum',
  photo_url varchar(255) DEFAULT 'https://gitlab.com/sea-eevee/'
);
INSERT INTO merchant_profile(merchant_id, location_id, display_name) VALUES (1, 1, 'Toko John Doe');
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (2, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (3, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (4, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (5, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (6, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (7, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (8, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (9, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (10, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (11, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (12, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (13, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (14, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (15, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (16, 1);
INSERT INTO merchant_profile(merchant_id, location_id) VALUES (17, 1);

CREATE TABLE merchant_transfer (
  id BIGSERIAL PRIMARY KEY,
  merchant_id BIGINT NOT NULL, -- REFERENCES merchants (id),
  bank_name VARCHAR(255),
  bank_account_number VARCHAR(255),
  amount INT NOT NULL
);

CREATE TABLE item_detail (
  id BIGSERIAL PRIMARY KEY,
  merchant_id BIGINT NOT NULL ,
  display_name varchar(255) NOT NULL DEFAULT 'Random name',
  price int NOT NULL DEFAULT 1000,
  description varchar(255) NOT NULL DEFAULT 'lorem ipsum dolor sit amet',
  image varchar(255) NOT NULL DEFAULT 'https://d33v4339jhl8k0.cloudfront.net/docs/assets/5c814e0d2c7d3a0cb9325d1f/images/5c8bc20d2c7d3a154460eb97/file-1CjQ85QAme.jpg'
);
INSERT INTO item_detail(id, merchant_id, display_name, price, description, image) VALUES (1, 1, 'Dadu', 3000, 'Ini adalah sebuah dadu', 'https://sc01.alicdn.com/kf/H0a4dd4b62de04493a459ea4e900c8edfB.jpg_350x350.jpg');
INSERT INTO item_detail(id, merchant_id, display_name, price, description, image) VALUES (2, 1, 'Pizza', 50000, 'Ini adalah sebuah pizza', 'https://edscyclopedia.com/wp-content/uploads/2016/07/dadu_merah-300x296.png');
INSERT INTO item_detail(id, merchant_id, display_name, price, description, image) VALUES (3, 1, 'Meja', 200000, 'Ini adalah sebuah meja', 'https://cdn.monotaro.id/media/catalog/product/cache/6/image/b5fa40980320eb406ba395dece54e4a8/S/0/S004629955-1.jpg');
INSERT INTO item_detail(id, merchant_id, display_name, price, description, image) VALUES (4, 1, 'Kursi', 3000, 'Ini adalah sebuah kursi', 'https://static.bmdstatic.com/pk/product/medium/5bc7e4e6674f3.jpg');
INSERT INTO item_detail(id, merchant_id, display_name, price, description, image) VALUES (5, 2, 'Galaxi S', 1500000, 'lorem ipsum dolor sit amet', 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Samsung_Galaxy_S_White.png/160px-Samsung_Galaxy_S_White.png');
INSERT INTO item_detail(id, merchant_id, display_name, price, description, image) VALUES (6, 2, 'Galaxi Note', 2000000, 'lorem ipsum dolor sit amet', 'https://cdn.mos.cms.futurecdn.net/APEMG5VXxpduP6Py8AuGmm-1200-80.jpg');
INSERT INTO item_detail(id, merchant_id, display_name, price, description, image) VALUES (7, 3, 'Macbook Pro', 25000000, 'lorem ipsum dolor sit amet', 'https://9to5mac.com/wp-content/uploads/sites/6/2019/11/16-inch-MacBook-Pro-Top-Features-vs-13-inch-MacBook-Pro.jpg');
INSERT INTO item_detail(id, merchant_id, display_name, price, description, image) VALUES (8, 3, 'Macbook Air', 20000000, 'lorem ipsum dolor sit amet', 'https://www.notebookcheck.net/uploads/tx_nbc2/air13teaser.jpg');
INSERT INTO item_detail(id, merchant_id, display_name, price, description, image) VALUES (9, 3, 'Iphone 10', 12000000, 'lorem ipsum dolor sit amet', 'https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full//104/MTA-1655978/nillkin_nillkin-nature-tpu-casing-for-apple-iphone-x-or-apple-iphone-10---grey_full05.jpg');
INSERT INTO item_detail(id, merchant_id) VALUES (10, 4);
INSERT INTO item_detail(id, merchant_id) VALUES (11, 4);
INSERT INTO item_detail(id, merchant_id) VALUES (12, 4);
INSERT INTO item_detail(id, merchant_id) VALUES (13, 5);
INSERT INTO item_detail(id, merchant_id) VALUES (14, 5);
INSERT INTO item_detail(id, merchant_id) VALUES (15, 5);
INSERT INTO item_detail(id, merchant_id) VALUES (16, 6);
INSERT INTO item_detail(id, merchant_id) VALUES (17, 6);
INSERT INTO item_detail(id, merchant_id) VALUES (18, 6);
INSERT INTO item_detail(id, merchant_id) VALUES (19, 7);
INSERT INTO item_detail(id, merchant_id) VALUES (20, 8);
INSERT INTO item_detail(id, merchant_id) VALUES (21, 9);
INSERT INTO item_detail(id, merchant_id) VALUES (22, 10);
INSERT INTO item_detail(id, merchant_id) VALUES (23, 11);
INSERT INTO item_detail(id, merchant_id) VALUES (24, 12);
INSERT INTO item_detail(id, merchant_id) VALUES (25, 13);
INSERT INTO item_detail(id, merchant_id) VALUES (26, 14);
INSERT INTO item_detail(id, merchant_id) VALUES (27, 15);
INSERT INTO item_detail(id, merchant_id) VALUES (28, 16);
INSERT INTO item_detail(id, merchant_id) VALUES (29, 17);
select setval('item_detail_id_seq'::regclass,30,false);

CREATE TABLE merchant_transaction_history (
  id BIGSERIAL PRIMARY KEY,
  merchant_id BIGINT NOT NULL, -- REFERENCES merchants (id),
  transaction_id BIGINT NOT NULL,
  total_price BIGINT NOT NULL
);

CREATE TABLE merchant_request_item (
  item_id BIGINT,
  merchant_id bigint, -- REFERENCES merchant (id),
  quantity int DEFAULT 1
);
