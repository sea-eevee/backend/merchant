package api_merchant

import (
	"encoding/json"
	"gitlab.com/sea-eevee/backend/common/request_getter"
	"gitlab.com/sea-eevee/backend/merchant/pkg/responder"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
	"net/http"
)

func MyTransferCreate(ucFunc uc_merchant.MyTransferCreateUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		userID, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		param := new(uc_merchant.MyTransferCreateParam)
		err = json.NewDecoder(r.Body).Decode(param)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		param.MerchantID = userID
		resp, err := ucFunc(param)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
