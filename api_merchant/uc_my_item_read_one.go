package api_merchant

import (
	"gitlab.com/sea-eevee/backend/merchant/pkg/responder"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
	"net/http"
)

func MyItemReadOne(ucFunc uc_merchant.MyItemReadOneUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		resp, err := ucFunc(&uc_merchant.MyItemReadOneParam{
			ItemID: extractItemID(r),
		})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
