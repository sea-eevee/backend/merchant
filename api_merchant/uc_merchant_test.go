package api_merchant_test

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/sea-eevee/backend/merchant/api_merchant"

	"gitlab.com/sea-eevee/backend/merchant/pkg/dtos"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_merchant"
	"gitlab.com/sea-eevee/backend/merchant/repo/repo_oms"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
)

var mockItem = &repo_merchant.ItemDetail{
	MerchantID:  1,
	DisplayName: "lorem ipsum",
	Price:       50,
	Description: "lorem ipsum",
	Image:       "http",
}
var mockOMS = &repo_oms.MerchantItemStock{
	MerchantID: 1,
	ItemID:     2,
	Stock:      5,
}
var mockReqBody = &dtos.Item{
	Description: mockItem.Description,
	DisplayName: mockItem.DisplayName,
	Price:       mockItem.Price,
	Image:       mockItem.Image,
	MerchantID:  mockItem.ItemID,
	Stock:       mockOMS.Stock,
}

// func TestCreateItem(t *testing.T) {
// 	sampleBody, err := json.Marshal(mockReqBody)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	var tts = []struct {
// 		caseName    string
// 		handlerFunc uc_merchant.MyItemCreateUCFunc
// 		request     func() *http.Request
// 		result      func(resp *http.Response)
// 	}{
// 		{
// 			caseName: "when everything is all right should return 200 OK",
// 			handlerFunc: func(param *uc_merchant.MyItemCreateParam) (*uc_merchant.MyItemCreateResponse, error) {
// 				assert.Equal(t, mockItem.MerchantID, param.MerchantID)
// 				return nil, nil
// 			},
// 			request: func() *http.Request {
// 				r, _ := http.NewRequest(http.MethodPost, "/item", bytes.NewBuffer(sampleBody))
// 				r.Header.Set("X-UserID", "1")
// 				return r
// 			},
// 			result: func(resp *http.Response) {
// 				assert.Equal(t, http.StatusOK, resp.StatusCode)
// 			},
// 		},
// 	}

// 	for _, tt := range tts {
// 		t.Log(tt.caseName)

// 		router := mux.NewRouter()

// 		router.Path("/item").Handler(api_merchant.MyItemCreate(tt.handlerFunc))

// 		rr := httptest.NewRecorder()

// 		req := tt.request()
// 		router.ServeHTTP(rr, req)

// 		tt.result(rr.Result())
// 	}
// }

func TestDeleteItem(t *testing.T) {
	itemDelete := &uc_merchant.MyItemDeleteParam{
		MerchantID: 1,
		ItemID:     1,
	}
	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.MyItemDeleteUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_merchant.MyItemDeleteParam) (*uc_merchant.MyItemDeleteResponse, error) {
				assert.Equal(t, itemDelete.ItemID, param.ItemID)
				return nil, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPost, "/item/1", bytes.NewBuffer(nil))
				r.Header.Set("X-UserID", "1")
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/item/{item_id}").Handler(api_merchant.MyItemDelete(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}

func TestReadManyItem(t *testing.T) {
	itemDelete := &uc_merchant.MyItemReadManyParam{
		MerchantID: 1,
	}
	body, err := json.Marshal(itemDelete)
	if err != nil {
		log.Fatal(err)
	}
	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.MyItemReadManyUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_merchant.MyItemReadManyParam) (*uc_merchant.MyItemReadManyResponse, error) {
				assert.Equal(t, itemDelete.MerchantID, param.MerchantID)
				return nil, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, "/item", bytes.NewBuffer(body))
				r.Header.Set("X-UserID", "1")
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/item").Handler(api_merchant.MyItemReadMany(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}

func TestReadOneItem(t *testing.T) {
	itemDelete := &uc_merchant.MyItemReadOneParam{
		ItemID: 1,
	}
	body, err := json.Marshal(itemDelete)
	if err != nil {
		log.Fatal(err)
	}
	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.MyItemReadOneUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_merchant.MyItemReadOneParam) (*uc_merchant.MyItemReadOneResponse, error) {
				assert.Equal(t, itemDelete.ItemID, param.ItemID)
				return nil, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, "/item/1", bytes.NewBuffer(body))
				r.Header.Set("X-UserID", "1")
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/item/{item_id}").Handler(api_merchant.MyItemReadOne(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}

func TestStockUpdateItem(t *testing.T) {
	itemDelete := &uc_merchant.MyItemStockUpdateParam{
		ItemID:     1,
		MerchantID: 1,
		Difference: 5,
	}
	body, err := json.Marshal(itemDelete)
	if err != nil {
		log.Fatal(err)
	}
	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.MyItemStockUpdateUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_merchant.MyItemStockUpdateParam) (*uc_merchant.MyItemStockUpdateResponse, error) {
				assert.Equal(t, itemDelete.ItemID, param.ItemID)
				return nil, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPut, "/item/1/stock/5", bytes.NewBuffer(body))
				r.Header.Set("X-UserID", "1")
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/item/{item_id}/stock/{stock_id}").Handler(api_merchant.MyItemStockUpdate(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}

func TestUpdateItem(t *testing.T) {
	itemDelete := &uc_merchant.MyItemUpdateParam{
		ItemID:     1,
		MerchantID: 1,
		ItemDetail: &dtos.Item{},
	}
	body, err := json.Marshal(itemDelete)
	if err != nil {
		log.Fatal(err)
	}
	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.MyItemUpdateUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_merchant.MyItemUpdateParam) (*uc_merchant.MyItemUpdateResponse, error) {
				assert.Equal(t, itemDelete.ItemID, param.ItemID)
				return nil, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPut, "/item/1", bytes.NewBuffer(body))
				r.Header.Set("X-UserID", "1")
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/item/{item_id}").Handler(api_merchant.MyItemUpdate(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}

func TestWalletBalance(t *testing.T) {
	itemDelete := &uc_merchant.MyRealWalletBalanceReadParam{
		MerchantID: 1,
	}
	body, err := json.Marshal(itemDelete)
	if err != nil {
		log.Fatal(err)
	}
	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.MyRealWalletBalanceReadUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_merchant.MyRealWalletBalanceReadParam) (*uc_merchant.MyRealWalletBalanceReadResponse, error) {
				assert.Equal(t, itemDelete.MerchantID, param.MerchantID)
				return nil, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, "/transfer/wallet", bytes.NewBuffer(body))
				r.Header.Set("X-UserID", "1")
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/transfer/wallet").Handler(api_merchant.MyRealWalletBalanceRead(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}

func TestTransferCreate(t *testing.T) {
	itemDelete := &uc_merchant.MyTransferCreateParam{
		MerchantID:            1,
		MerchantBankAccountID: 1,
		Amount:                10,
	}
	body, err := json.Marshal(itemDelete)
	if err != nil {
		log.Fatal(err)
	}
	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.MyTransferCreateUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_merchant.MyTransferCreateParam) (*uc_merchant.MyTransferCreateResponse, error) {
				assert.Equal(t, itemDelete.MerchantID, param.MerchantID)
				return nil, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPost, "/transfer", bytes.NewBuffer(body))
				r.Header.Set("X-UserID", "1")
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/transfer").Handler(api_merchant.MyTransferCreate(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}

func TestProfileSee(t *testing.T) {
	itemDelete := &uc_merchant.ProfileSeeParam{
		MerchantID: 1,
	}
	body, err := json.Marshal(itemDelete)
	if err != nil {
		log.Fatal(err)
	}
	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.ProfileSeeUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_merchant.ProfileSeeParam) (*uc_merchant.ProfileSeeResponse, error) {
				assert.Equal(t, itemDelete.MerchantID, param.MerchantID)
				return nil, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, "/profile", bytes.NewBuffer(body))
				r.Header.Set("X-UserID", "1")
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/profile").Handler(api_merchant.ProfileSee(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}

func TestTransferReadMany(t *testing.T) {
	itemDelete := &uc_merchant.MyTransferReadManyParam{
		MerchantID: 1,
	}
	body, err := json.Marshal(itemDelete)
	if err != nil {
		log.Fatal(err)
	}
	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.MyTransferReadManyUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_merchant.MyTransferReadManyParam) (*uc_merchant.MyTransferReadManyResponse, error) {
				assert.Equal(t, itemDelete.MerchantID, param.MerchantID)
				return nil, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, "/transfer", bytes.NewBuffer(body))
				r.Header.Set("X-UserID", "1")
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/transfer").Handler(api_merchant.MyTransferReadMany(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}
