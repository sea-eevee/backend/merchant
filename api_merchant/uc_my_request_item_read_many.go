package api_merchant

import (
	"encoding/json"
	"net/http"

	"gitlab.com/sea-eevee/backend/common/request_getter"
	"gitlab.com/sea-eevee/backend/merchant/pkg/responder"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
)

func MyRequestItemReadMany(ucFunc uc_merchant.MyRequestItemReadManyUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		userID, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		param := new(uc_merchant.MyRequestItemReadManyParam)
		err = json.NewDecoder(r.Body).Decode(param)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		param.MerchantID = userID
		resp, err := ucFunc(param)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
