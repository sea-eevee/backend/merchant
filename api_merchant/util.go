package api_merchant

import (
	"bytes"
	"fmt"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/mitchellh/mapstructure"

	"github.com/google/uuid"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"

	"github.com/gorilla/mux"
)

const (
	URLKeyItemID = "item_id"
	StockUpdate  = "stock"
	URLKeyBankID = "bank_id"
	maxSize      = int64(5120000)
	bucket       = "sea-marketplace"
)

type FileForm struct {
	session    *session.Session
	file       multipart.File
	fileHeader *multipart.FileHeader
}

func extractItemID(r *http.Request) uint64 {
	raw := mux.Vars(r)[URLKeyItemID]
	id, err := strconv.ParseUint(raw, 10, 64)
	if err != nil {
		return 0
	}
	return id
}

func extractBankID(r *http.Request) uint64 {
	raw := mux.Vars(r)[URLKeyBankID]
	id, err := strconv.ParseUint(raw, 10, 64)
	if err != nil {
		return 0
	}
	return id
}

func extractStock(r *http.Request) int {
	raw := mux.Vars(r)[StockUpdate]
	id, err := strconv.Atoi(raw)
	if err != nil {
		return 0
	}
	return id
}

func handlingMultipartForm(r *http.Request, ID uint64, folder string) (map[string]interface{}, error) {
	form := make(map[string]interface{})
	err := r.ParseMultipartForm(maxSize)
	if err != nil {
		return nil, err
	}
	f := new(FileForm)
	f.file, f.fileHeader, err = r.FormFile("picture")
	if err != nil {
		return nil, err
	}
	defer f.file.Close()
	f.session, err = initializeSessionAWS()
	if err != nil {
		return nil, err
	}

	fileName, err := uploadFileToS3(f, ID, folder)
	form["image"] = fileName

	for key, values := range r.Form {
		for _, value := range values {
			form[key] = value
		}
	}
	return form, nil
}

func initializeSessionAWS() (*session.Session, error) {
	region := "us-east-1"
	secretAccessKey := "NGxh1zFO5L54f3KgIgKoaNepBI6l9THeWjqRQpkK"
	sessionToken := "FwoGZXIvYXdzEKX//////////wEaDL/93TJ55p3SG92KbSLJAQaf2ooTRUSXsV3zOQNkRRcBVZgsavWTJTAdYTjuAVnACX9YU859Jj3phdtp0M2qfK4i7g4uJjmuKFWrm4z9yFwk6igb7u3yEdqFnHZYjpX/0cn3gRE8Hs+Exe/r9SFh+KE/+OM0nN/CgTmNHXImg08WFHPmj0iByJheOvgr0rKSCY/Rw5eoSGdvsktEwwytkmvnam0G7BNitKx+9V+uk+jEvvMOixO2GtcMui87yys9CB2MTsIF72VUu2kZU+gZk8IZbkKlJGSW9yiTovb6BTItS4Ucfm5MBq+1MW6Cs6qDXgty2nlXj5i38Smcdl2mvZDpAVRYRlC2OrzpWU2p"
	accessKeyID := "ASIAWCX7PNYOACKZTVDV"
	fmt.Println(os.Environ())
	return session.NewSession(&aws.Config{
		Region: aws.String(region),
		Credentials: credentials.NewStaticCredentials(
			accessKeyID,     // id
			secretAccessKey, // secret
			sessionToken),
	})
}

func uploadFileToS3(fileF *FileForm, ID uint64, folder string) (string, error) {
	size := fileF.fileHeader.Size
	buffer := make([]byte, size)
	fileF.file.Read(buffer)
	tempFileName := fmt.Sprintf("%v/%v-%v%v", folder, strconv.FormatUint(ID, 10), uuid.New().String(), filepath.Ext(fileF.fileHeader.Filename))
	_, err := s3.New(fileF.session).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(bucket),
		Key:                  aws.String(tempFileName),
		ACL:                  aws.String("public-read"), // could be private if you want it to be access by only authorized users
		Body:                 bytes.NewReader(buffer),
		ContentLength:        aws.Int64(int64(size)),
		ContentType:          aws.String(http.DetectContentType(buffer)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
		StorageClass:         aws.String("INTELLIGENT_TIERING"),
	})
	if err != nil {
		return "", err
	}
	url := fmt.Sprintf("https://%v.s3.amazonaws.com/%v", bucket, tempFileName)
	return url, err
}

func decodeForm(conf *mapstructure.DecoderConfig, form map[string]interface{}) error {
	decoder, err := mapstructure.NewDecoder(conf)
	if err != nil {
		panic(err)
	}
	err = decoder.Decode(form)
	if err != nil {
		return err
	}
	return err
}
