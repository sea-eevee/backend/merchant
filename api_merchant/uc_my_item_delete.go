package api_merchant

import (
	"gitlab.com/sea-eevee/backend/common/request_getter"
	"gitlab.com/sea-eevee/backend/merchant/pkg/responder"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
	"net/http"
)

func MyItemDelete(ucFunc uc_merchant.MyItemDeleteUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		userID, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		resp, err := ucFunc(&uc_merchant.MyItemDeleteParam{
			MerchantID: userID,
			ItemID:     extractItemID(r),
		})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
