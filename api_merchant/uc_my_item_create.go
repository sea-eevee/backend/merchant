package api_merchant

import (
	"net/http"

	"github.com/mitchellh/mapstructure"

	"gitlab.com/sea-eevee/backend/common/request_getter"
	"gitlab.com/sea-eevee/backend/merchant/pkg/dtos"
	"gitlab.com/sea-eevee/backend/merchant/pkg/responder"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
)

func MyItemCreate(ucFunc uc_merchant.MyItemCreateUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userID, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		updatedItem := new(dtos.Item)
		const it = "item"
		itemForm, err := handlingMultipartForm(r, userID, it)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		config := &mapstructure.DecoderConfig{
			WeaklyTypedInput: true,
			Result:           &updatedItem,
		}
		err = decodeForm(config, itemForm)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		resp, err := ucFunc(&uc_merchant.MyItemCreateParam{
			MerchantID: userID,
			ItemDetail: updatedItem,
		})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		responder.ResponseOK(w, resp)
	})
}
