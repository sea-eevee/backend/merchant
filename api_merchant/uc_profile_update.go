package api_merchant

import (
	"net/http"

	"github.com/mitchellh/mapstructure"

	"gitlab.com/sea-eevee/backend/common/request_getter"

	"gitlab.com/sea-eevee/backend/merchant/pkg/responder"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
)

func ProfileUpdate(ucFunc uc_merchant.ProfileUpdateUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userID, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		param := new(uc_merchant.ProfileUpdateParam)
		param.MerchantID = userID
		const it = "profile"
		profileForm, err := handlingMultipartForm(r, userID, it)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		config := &mapstructure.DecoderConfig{
			WeaklyTypedInput: true,
			Result:           &param,
		}
		err = decodeForm(config, profileForm)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		resp, err := ucFunc(param)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		responder.ResponseOK(w, resp)
	})
}
