package api_merchant

import (
	"net/http"

	"gitlab.com/sea-eevee/backend/common/request_getter"
	"gitlab.com/sea-eevee/backend/merchant/pkg/responder"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
)

func MyItemStockUpdate(ucFunc uc_merchant.MyItemStockUpdateUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userID, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		resp, err := ucFunc(&uc_merchant.MyItemStockUpdateParam{
			MerchantID: userID,
			ItemID:     extractItemID(r),
			Difference: extractStock(r),
		})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
