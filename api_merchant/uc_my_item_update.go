package api_merchant

import (
	"encoding/json"
	"net/http"

	"gitlab.com/sea-eevee/backend/common/request_getter"
	"gitlab.com/sea-eevee/backend/merchant/pkg/dtos"
	"gitlab.com/sea-eevee/backend/merchant/pkg/responder"
	"gitlab.com/sea-eevee/backend/merchant/uc_merchant"
)

func MyItemUpdate(ucFunc uc_merchant.MyItemUpdateUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userID, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		paramItemUpdated := new(dtos.Item)
		err = json.NewDecoder(r.Body).Decode(paramItemUpdated)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		resp, err := ucFunc(&uc_merchant.MyItemUpdateParam{
			MerchantID: userID,
			ItemID:     extractItemID(r),
			ItemDetail: paramItemUpdated,
		})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
