module gitlab.com/sea-eevee/backend/merchant

go 1.15

require (
	github.com/aws/aws-sdk-go v1.34.19
	github.com/golang/mock v1.4.3
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/stretchr/testify v1.5.1
	gitlab.com/sea-eevee/backend/common v1.0.1
	gitlab.com/sea-eevee/marketplace-be v0.0.0-20200901145654-56ab5fd40841
)
