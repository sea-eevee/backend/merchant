package dtos

type Item struct {
	ItemID      uint64 `json:"item_id" mapstructure:"id"`
	MerchantID  uint64 `json:"merchant_id" mapstructure:"merchant_id"`
	Stock       uint   `json:"stock" mapstructure:"stock"`
	DisplayName string `json:"display_name" mapstructure:"display_name"`
	Price       uint   `json:"price" mapstructure:"price"`
	Description string `json:"description" mapstructure:"description"`
	Image       string `json:"image" mapstructure:"image"`
}

type RequestItem struct {
	Item     Item `json:"item"`
	Quantity uint `json:"quantity"`
}
