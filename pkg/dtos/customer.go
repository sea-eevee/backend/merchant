package dtos

type Customer struct {
	ID                 uint64
	Username           string
	DisplayName        string
	ShoppingCart       []Item
	TransactionHistory []Transaction
}
