.PHONY : network-create db-reload app-build app-run app-stop app-clean

network:
	docker network inspect compfest-marketplace >/dev/null 2>&1 || \
        docker network create --driver bridge compfest-marketplace

db-reload:
	docker container stop compfest-marketplace-service-merchant-db || true
	docker build -t compfest-marketplace-service-merchant-db-img -f Dockerfile.postgres .
	docker run -d --rm --name compfest-marketplace-service-merchant-db \
        --network=compfest-marketplace \
        -p 13200:5432 \
        -v compfest-marketplace-merchant-vol:/var/lib/postgresql/data \
        -e POSTGRES_USER=pgku \
        -e POSTGRES_PASSWORD=pgku \
        -e POSTGRES_DB=merchant \
        compfest-marketplace-service-merchant-db-img

app-build:
	docker build -t compfest-marketplace-service-merchant -f Dockerfile.app .

app-run:
	docker run -d --rm --name compfest-marketplace-service-merchant \
	--network=compfest-marketplace \
	-p 8200:8200 \
	compfest-marketplace-service-merchant

app-stop:
	docker stop compfest-marketplace-service-merchant || true

app-reload:
	make app-stop
	make app-build
	make app-run

app-clean:
	docker rmi -f compfest-marketplace-service-merchant

full-reload:
	make db-reload
	make app-reload